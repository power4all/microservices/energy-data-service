﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EnergyDataService.Api.Logic.Hubs
{
	public class EnergyConsumptionTotalsHub : Hub
	{
		private readonly ILogger<EnergyConsumptionTotalsHub> _logger;

		public EnergyConsumptionTotalsHub(
			ILogger<EnergyConsumptionTotalsHub> logger
		)
		{
			_logger = logger;
		}

		public override async Task OnConnectedAsync()
		{
			await base.OnConnectedAsync();
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			await base.OnDisconnectedAsync(exception);
		}
	}
}