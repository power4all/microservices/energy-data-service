﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EnergyDataService.Api.Logic.Hubs
{
	public class EnergyConsumptionHub : Hub
	{
		private readonly ILogger<EnergyConsumptionHub> _logger;

		public EnergyConsumptionHub(
			ILogger<EnergyConsumptionHub> logger
		)
		{
			_logger = logger;
		}

		public override async Task OnConnectedAsync()
		{
			await base.OnConnectedAsync();
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			await base.OnDisconnectedAsync(exception);
		}
	}
}