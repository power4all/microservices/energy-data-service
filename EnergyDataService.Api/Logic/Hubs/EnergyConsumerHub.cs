﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EnergyDataService.Api.Logic.Hubs
{
	public class EnergyConsumerHub : Hub
	{
		private readonly ILogger<EnergyConsumptionHub> _logger;

		public EnergyConsumerHub(
			ILogger<EnergyConsumptionHub> logger
		)
		{
			_logger = logger;
		}

		public override async Task OnConnectedAsync()
		{
			await base.OnConnectedAsync();
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			await base.OnDisconnectedAsync(exception);
		}
	}
}