using EnergyDataService.Api.Data;
using EnergyDataService.Api.Models.Entity;
using System;
using System.Linq;

namespace EnergyDataService.Api.Logic
{
	public class ConsumerTotals
	{
		private readonly ApplicationDbContext _context;

		public ConsumerTotals(
			ApplicationDbContext context
		)
		{
			_context = context;
		}

		public ConsumerTotalEnergyMessage Day()
		{
			return CalculateTotal(1);
		}

		public ConsumerTotalEnergyMessage Week()
		{
			return CalculateTotal(7);
		}

		public ConsumerTotalEnergyMessage Month()
		{
			return CalculateTotal(30);
		}

		private ConsumerTotalEnergyMessage CalculateTotal(int days)
		{
			var day = DateTime.Today.AddDays(-days);

			var totals = _context.ConsumerTotalEnergyMessages
				.Where(m => m.DateTime >= day)
				.ToList();

			long totalConsumption = 0;
			long totalProduction = 0;

			foreach (var message in totals)
			{
				totalConsumption += message.TotalConsumption;
				totalProduction += message.TotalProduction;
			}

			var avgConsumption = totalConsumption / totals.Count;
			var avgProduction = totalProduction / totals.Count;
			var resultConsumption = (days * 24) * avgConsumption;
			var resultProduction = (days * 24) * avgProduction;

			return new ConsumerTotalEnergyMessage
			{
				TotalConsumption = resultConsumption,
				TotalProduction = resultProduction
			};
		}
	}
}