using EnergyDataService.Api.Data;
using System;
using System.Linq;

namespace EnergyDataService.Api.Logic
{
	public class ProducerTotals
	{
		private readonly ApplicationDbContext _context;

		public ProducerTotals(
			ApplicationDbContext context
		)
		{
			_context = context;
		}

		public long Day()
		{
			return CalculateTotal(1);
		}

		public long Week()
		{
			return CalculateTotal(7);
		}

		public long Month()
		{
			return CalculateTotal(30);
		}

		private long CalculateTotal(int days)
		{
			var day = DateTime.Today.AddDays(-days);

			var totals = _context.ProducerTotalEnergyMessages
				.Where(m => m.DateTime >= day)
				.ToList();

			long total = 0;
			foreach (var message in totals)
			{
				total += message.TotalOutput;
			}

			var avg = total / totals.Count;
			var result = (days * 24) * avg;

			return result;
		}
	}
}