using EnergyDataService.Api.Data;
using EnergyDataService.Api.Logic;
using EnergyDataService.Api.Logic.Hubs;
using EnergyDataService.Api.Models.Entity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Text;

namespace EnergyDataService.Api.RabbitMQEventbus
{
	public class ProducerTotalMessageHandler
	{
		private readonly IHubContext<EnergyProductionTotalsHub> _hubContext;
		private readonly IServiceProvider _serviceProvider;

		public ProducerTotalMessageHandler(
			IHubContext<EnergyProductionTotalsHub> hubContext,
			IServiceProvider serviceProvider
			)
		{
			_hubContext = hubContext;
			_serviceProvider = serviceProvider;
		}

		public RabbitMQMessage HandleTotalEnergyDataProducer(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var messageString = Encoding.UTF8.GetString(body);

			try
			{
				var message = JsonConvert.DeserializeObject<ProducerTotalEnergyMessage>(messageString);

				if (message != null)
				{
					using (var scope = _serviceProvider.CreateScope())
					{
						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
						var producerTotals = scope.ServiceProvider.GetRequiredService<ProducerTotals>();

						context.ProducerTotalEnergyMessages.Add(message);
						context.SaveChanges();

						_hubContext.Clients.All.SendAsync("ProducerTotalCurrent", message);

						var day = new ProducerTotalEnergyMessage
						{
							TotalOutput = producerTotals.Day()
						};

						_hubContext.Clients.All.SendAsync("ProducerTotalDay", day);

						var week = new ProducerTotalEnergyMessage
						{
							TotalOutput = producerTotals.Week()
						};
						_hubContext.Clients.All.SendAsync("ProducerTotalWeek", week);

						var month = new ProducerTotalEnergyMessage
						{
							TotalOutput = producerTotals.Month()
						};
						_hubContext.Clients.All.SendAsync("ProducerTotalMonth", month);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}

			return null;
		}
	}
}