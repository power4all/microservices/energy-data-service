using EnergyDataService.Api.Data;
using EnergyDataService.Api.Logic.Hubs;
using EnergyDataService.Api.Models.Entity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Text;

namespace EnergyDataService.Api.RabbitMQEventbus
{
	public class ProducerMessageHandler
	{
		private readonly IHubContext<EnergyProductionHub> _hubContext;
		private readonly IServiceProvider _serviceProvider;

		public ProducerMessageHandler(
			IHubContext<EnergyProductionHub> hubContext,
			IServiceProvider serviceProvider
			)
		{
			_hubContext = hubContext;
			_serviceProvider = serviceProvider;
		}

		public RabbitMQMessage HandleSubtotalEnergyDataProducer(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var messageString = Encoding.UTF8.GetString(body);

			try
			{
				var message = JsonConvert.DeserializeObject<ProducerSubtotalEnergyMessage>(messageString);

				if (message != null)
				{
					using (var scope = _serviceProvider.CreateScope())
					{
						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

						context.ProducerSubtotalEnergyMessages.Add(message);
						context.SaveChanges();

						_hubContext.Clients.All.SendAsync("Producer", message);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}

			return null;
		}
	}
}