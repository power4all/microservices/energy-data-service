//using System;
//using System.Text;
//using EnergyDataService.Api.Data;
//using EnergyDataService.Api.Logic;
//using EnergyDataService.Api.Logic.Hubs;
//using EnergyDataService.Api.Models.Entity;
//using Microsoft.AspNetCore.SignalR;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
//using RabbitMQ.Client.Events;
//using RabbitMQ_Eventbus.Message;

//namespace EnergyDataService.Api.RabbitMQEventbus
//{
//	public class ConsumerMessageHandler
//	{
//		private readonly IHubContext<EnergyConsumptionHub> _hubContext;
//		private readonly IServiceProvider _serviceProvider;

//		public ConsumerMessageHandler(
//			IHubContext<EnergyConsumptionHub> hubContext,
//			IServiceProvider serviceProvider
//			)
//		{
//			_hubContext = hubContext;
//			_serviceProvider = serviceProvider;
//		}

//		public RabbitMQMessage HandleTotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs)
//		{
//			var body = eventArgs.Body.ToArray();
//			var messageString = Encoding.UTF8.GetString(body);

//			try
//			{
//				var message = JsonConvert.DeserializeObject<ConsumerTotalEnergyMessage>(messageString);

//				if (message != null)
//				{
//					using (var scope = _serviceProvider.CreateScope())
//					{
//						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
//						var producerTotals = scope.ServiceProvider.GetRequiredService<ConsumerTotals>();

//						context.ConsumerTotalEnergyMessages.Add(message);
//						context.SaveChanges();

//						_hubContext.Clients.All.SendAsync("Consumer", message);
//					}
//				}
//			}
//			catch (Exception e)
//			{
//				Console.WriteLine(e.StackTrace);
//			}

//			return null;
//		}
//	}
//}