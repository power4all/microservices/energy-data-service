using EnergyDataService.Api.Data;
using EnergyDataService.Api.Logic;
using EnergyDataService.Api.Logic.Hubs;
using EnergyDataService.Api.Models.Entity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Text;

namespace EnergyDataService.Api.RabbitMQEventbus
{
	public class NetbeheerderConsumerMessageHandler
	{
		private readonly IHubContext<EnergyConsumptionHub> _consumptionHub;
		private readonly IHubContext<EnergyConsumptionTotalsHub> _consumptionTotalsHub;
		private readonly IServiceProvider _serviceProvider;

		public NetbeheerderConsumerMessageHandler(
			IHubContext<EnergyConsumptionHub> consumptionHub,
			IHubContext<EnergyConsumptionTotalsHub> consumptionTotalsHub,
			IServiceProvider serviceProvider
			)
		{
			_consumptionHub = consumptionHub;
			_consumptionTotalsHub = consumptionTotalsHub;
			_serviceProvider = serviceProvider;
		}

		public RabbitMQMessage HandleTotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var messageString = Encoding.UTF8.GetString(body);

			try
			{
				var message = JsonConvert.DeserializeObject<ConsumerTotalEnergyMessage>(messageString);

				if (message != null)
				{
					using (var scope = _serviceProvider.CreateScope())
					{
						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
						var consumerTotals = scope.ServiceProvider.GetRequiredService<ConsumerTotals>();

						context.ConsumerTotalEnergyMessages.Add(message);
						context.SaveChanges();

						_consumptionTotalsHub.Clients.All.SendAsync("ConsumerTotalCurrent", message);

						var day = consumerTotals.Day();

						_consumptionTotalsHub.Clients.All.SendAsync("ConsumerTotalDay", day);

						var week = consumerTotals.Week();
						_consumptionTotalsHub.Clients.All.SendAsync("ConsumerTotalWeek", week);

						var month = consumerTotals.Month();

						_consumptionTotalsHub.Clients.All.SendAsync("ConsumerTotalMonth", month);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			return null;
		}

		public RabbitMQMessage HandleSubtotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var messageString = Encoding.UTF8.GetString(body);

			try
			{
				var message = JsonConvert.DeserializeObject<ConsumerSubtotalEnergyMessage>(messageString);

				if (message != null)
				{
					using (var scope = _serviceProvider.CreateScope())
					{
						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

						context.ConsumerSubtotalEnergyMessages.Add(message);
						context.SaveChanges();

						_consumptionHub.Clients.All.SendAsync("ConsumerSubtotal", message);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}

			return null;
		}

		public RabbitMQMessage HandleEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var messageString = Encoding.UTF8.GetString(body);

			try
			{
				var message = JsonConvert.DeserializeObject<ConsumerMessage>(messageString);

				if (message != null)
				{
					using (var scope = _serviceProvider.CreateScope())
					{
						var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

						context.ConsumerMessages.Add(message);
						context.SaveChanges();

						_consumptionHub.Clients.All.SendAsync("Consumer", message);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}

			return null;
		}
	}
}