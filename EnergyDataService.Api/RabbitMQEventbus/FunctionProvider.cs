﻿using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Collections.Generic;

namespace EnergyDataService.Api.RabbitMQEventbus
{
	public class FunctionProvider : RabbitMQFunctionProviderBase
	{
		private ProducerMessageHandler _producerMessageHandler;
		private ProducerTotalMessageHandler _producerTotalMessageHandler;
		private NetbeheerderConsumerMessageHandler _netbeheerderConsumerMessageHandler;

		public FunctionProvider(
			NetbeheerderConsumerMessageHandler netbeheerderConsumerMessageHandler,
			ProducerMessageHandler producerMessageHandler,
			ProducerTotalMessageHandler producerTotalMessageHandler)
		{
			_netbeheerderConsumerMessageHandler = netbeheerderConsumerMessageHandler;
			_producerMessageHandler = producerMessageHandler;
			_producerTotalMessageHandler = producerTotalMessageHandler;
		}

		protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>
			InializeFunctionsWithKeys()
		{
			var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
			dictionary.Add(
				nameof(HandleTotalEnergyDataProducer), HandleTotalEnergyDataProducer);
			dictionary.Add(
				nameof(HandleSubtotalEnergyDataProducer), HandleSubtotalEnergyDataProducer);
			//dictionary.Add(
			//    nameof(HandleTotalEnergyDataConsumer), HandleTotalEnergyDataConsumer);
			dictionary.Add(
				nameof(HandleTotalEnergyDataConsumer), HandleTotalEnergyDataConsumer);
			dictionary.Add(
				 nameof(HandleSubtotalEnergyDataConsumer), HandleSubtotalEnergyDataConsumer);
			dictionary.Add(
				 nameof(HandleEnergyDataConsumer), HandleEnergyDataConsumer);

			return dictionary;
		}

		public RabbitMQMessage HandleTotalEnergyDataProducer(object o, BasicDeliverEventArgs eventArgs) =>
			_producerTotalMessageHandler.HandleTotalEnergyDataProducer(o, eventArgs);

		public RabbitMQMessage HandleSubtotalEnergyDataProducer(object o, BasicDeliverEventArgs eventArgs) =>
			_producerMessageHandler.HandleSubtotalEnergyDataProducer(o, eventArgs);

		//    public RabbitMQMessage HandleTotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs) =>
		//_consumerMessageHandler.HandleTotalEnergyDataConsumer(o, eventArgs);

		public RabbitMQMessage HandleTotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs) =>
			_netbeheerderConsumerMessageHandler.HandleTotalEnergyDataConsumer(o, eventArgs);

		public RabbitMQMessage HandleSubtotalEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs) =>
			_netbeheerderConsumerMessageHandler.HandleSubtotalEnergyDataConsumer(o, eventArgs);

		public RabbitMQMessage HandleEnergyDataConsumer(object o, BasicDeliverEventArgs eventArgs) =>
			_netbeheerderConsumerMessageHandler.HandleEnergyDataConsumer(o, eventArgs);
	}
}