using EnergyDataService.Api.Data;
using EnergyDataService.Api.Models.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EnergyDataService.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProducerMessagesController : ControllerBase
	{
		private readonly ApplicationDbContext _context;

		public ProducerMessagesController(
			ApplicationDbContext context
		)
		{
			_context = context;
		}

		// GET: api/Tests
		[HttpGet("subtotal")]
		public List<ProducerSubtotalEnergyMessage> GetSubtotalEnergyMessages()
		{
			return _context.ProducerSubtotalEnergyMessages.OrderByDescending(m => m.DateTime).Take(180).ToList();
		}

		// GET: api/Tests
		[HttpGet("subtotal/nuclear")]
		public List<ProducerSubtotalEnergyMessage> GetSubtotalNuclearEnergyMessages()
		{
			return _context.ProducerSubtotalEnergyMessages
				.Where(m => m.ProducerType.Equals("Nuclear"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}

		// GET: api/Tests
		[HttpGet("subtotal/solar")]
		public List<ProducerSubtotalEnergyMessage> GetSubtotalSolarEnergyMessages()
		{
			return _context.ProducerSubtotalEnergyMessages
				.Where(m => m.ProducerType.Equals("Solar"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}

		// GET: api/Tests
		[HttpGet("subtotal/wind")]
		public List<ProducerSubtotalEnergyMessage> GetSubtotalWindEnergyMessages()
		{
			return _context.ProducerSubtotalEnergyMessages
				.Where(m => m.ProducerType.Equals("Wind"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}
	}
}