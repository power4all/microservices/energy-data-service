using EnergyDataService.Api.Data;
using EnergyDataService.Api.Models.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace EnergyDataService.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ConsumerMessagesController : ControllerBase
	{
		private readonly ApplicationDbContext _context;

		public ConsumerMessagesController(
			ApplicationDbContext context
		)
		{
			_context = context;
		}

		[HttpGet("consumer/{id}")]
		public List<ConsumerMessage> GetSubtotalEnergyMessages(int id)
		{
			return _context.ConsumerMessages.OrderByDescending(m => m.DateTime)
				.Where(
					m => m.ConsumerId.Equals(id)
				).Take(180).ToList();
		}

		[HttpGet("subtotal")]
		public List<ConsumerSubtotalEnergyMessage> GetSubtotalEnergyMessages()
		{
			return _context.ConsumerSubtotalEnergyMessages.OrderByDescending(m => m.DateTime).Take(180).ToList();
		}

		[HttpGet("subtotal/small")]
		public List<ConsumerSubtotalEnergyMessage> GetSubtotalSmallConsumers()
		{
			return _context.ConsumerSubtotalEnergyMessages
				.Where(m => m.ConsumerType.Equals("small"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}

		[HttpGet("subtotal/medium")]
		public List<ConsumerSubtotalEnergyMessage> GetSubtotalMediumConsumers()
		{
			return _context.ConsumerSubtotalEnergyMessages
				.Where(m => m.ConsumerType.Equals("medium"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}

		[HttpGet("subtotal/large")]
		public List<ConsumerSubtotalEnergyMessage> GetSubtotalLargeConsumers()
		{
			return _context.ConsumerSubtotalEnergyMessages
				.Where(m => m.ConsumerType.Equals("large"))
				.OrderByDescending(m => m.DateTime)
				.Take(180).ToList();
		}
	}
}