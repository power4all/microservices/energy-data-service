﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace EnergyDataService.Api.Migrations
{
	public partial class initial : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "ConsumerMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					Production = table.Column<double>(type: "double", nullable: false),
					Consumption = table.Column<double>(type: "double", nullable: false),
					ConsumerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ConsumerId = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerMessages", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ConsumerSubtotalEnergyMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					TotalConsumption = table.Column<long>(type: "bigint", nullable: false),
					TotalProduction = table.Column<long>(type: "bigint", nullable: false),
					ConsumerCount = table.Column<long>(type: "bigint", nullable: false),
					ConsumerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerSubtotalEnergyMessages", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ConsumerTotalEnergyMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					TotalConsumption = table.Column<long>(type: "bigint", nullable: false),
					TotalProduction = table.Column<long>(type: "bigint", nullable: false),
					ConsumerCount = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerTotalEnergyMessages", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ProducerMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					Output = table.Column<double>(type: "double", nullable: false),
					ProducerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ProducerId = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerMessages", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ProducerSubtotalEnergyMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					TotalOutput = table.Column<long>(type: "bigint", nullable: false),
					ProducerCount = table.Column<long>(type: "bigint", nullable: false),
					ProducerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerSubtotalEnergyMessages", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ProducerTotalEnergyMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					TotalOutput = table.Column<long>(type: "bigint", nullable: false),
					ProducerCount = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerTotalEnergyMessages", x => x.Id);
				});
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "ConsumerMessages");

			migrationBuilder.DropTable(
				name: "ConsumerSubtotalEnergyMessages");

			migrationBuilder.DropTable(
				name: "ConsumerTotalEnergyMessages");

			migrationBuilder.DropTable(
				name: "ProducerMessages");

			migrationBuilder.DropTable(
				name: "ProducerSubtotalEnergyMessages");

			migrationBuilder.DropTable(
				name: "ProducerTotalEnergyMessages");
		}
	}
}