﻿using EnergyDataService.Api.Models.Entity;
using Microsoft.EntityFrameworkCore;

namespace EnergyDataService.Api.Data
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
		}

		public DbSet<ConsumerMessage> ConsumerMessages { get; set; }
		public DbSet<ProducerMessage> ProducerMessages { get; set; }

		public DbSet<ProducerTotalEnergyMessage> ProducerTotalEnergyMessages { get; set; }
		public DbSet<ProducerSubtotalEnergyMessage> ProducerSubtotalEnergyMessages { get; set; }
		public DbSet<ConsumerTotalEnergyMessage> ConsumerTotalEnergyMessages { get; set; }
		public DbSet<ConsumerSubtotalEnergyMessage> ConsumerSubtotalEnergyMessages { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			//optionsBuilder.UseLazyLoadingProxies();
			base.OnConfiguring(optionsBuilder);
		}
	}
}