using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataService.Api.Models.Entity
{
	public class ProducerMessage
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime DateTime { get; set; }

		public double Output { get; set; }

		public string ProducerType { get; set; }

		public long ProducerId { get; set; }
	}
}