﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataService.Api.Models.Entity
{
	public class ConsumerSubtotalEnergyMessage
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime DateTime { get; set; }

		public long TotalConsumption { get; set; }
		public long TotalProduction { get; set; }

		public long ConsumerCount { get; set; }
		public string ConsumerType { get; set; }
	}
}