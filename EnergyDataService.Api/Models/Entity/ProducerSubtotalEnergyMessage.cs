﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataService.Api.Models.Entity
{
	public class ProducerSubtotalEnergyMessage
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime DateTime { get; set; }

		public long TotalOutput { get; set; }
		public long ProducerCount { get; set; }
		public string ProducerType { get; set; }
	}
}