using EnergyDataService.Api.Data;
using EnergyDataService.Api.Helpers;
using EnergyDataService.Api.Logic;
using EnergyDataService.Api.Logic.Hubs;
using EnergyDataService.Api.RabbitMQEventbus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using RabbitMQEventbus.Extension.DependencyInjection;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.RabbitMQEventbusConfiguration;
using System;

namespace EnergyDataService.Api
{
	public class Startup
	{
		public Startup(IConfiguration configuration, IWebHostEnvironment env)
		{
			Configuration = configuration;
			Env = env;
		}

		public IConfiguration Configuration { get; }
		public IWebHostEnvironment Env { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseMySql(
					Configuration.GetConnectionString("MySql"),
					new MySqlServerVersion(new Version(8, 0, 23)),
					mySqlOptions =>
					{
						mySqlOptions.EnableRetryOnFailure(
							maxRetryCount: 2);
					});
			});

			services.Configure<RabbitMQEventbusConfiguration>(Configuration);
			services.AddOptions<RabbitMQEventbusConfiguration>();

			services.AddTransient<ProducerTotals>();
			services.AddSingleton<ProducerMessageHandler>();
			services.AddSingleton<NetbeheerderConsumerMessageHandler>();
			services.AddSingleton<ProducerTotalMessageHandler>();
			services.AddTransient<ConsumerTotals>();

			services.AddSingleton<IRabbitMQFunctionProvider>(s =>
			{
				return new FunctionProvider(

					s.GetService<NetbeheerderConsumerMessageHandler>(),
					s.GetService<ProducerMessageHandler>(),
					s.GetService<ProducerTotalMessageHandler>()
				);
			});
			services.AddRabbitMQEventbus(Env);

			services.AddControllers();
			services.AddSignalR();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "EnergyDataService.Api", Version = "v1" });
			});

			services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
			services.AddControllers(o =>
			{
				o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
			});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "EnergyDataService.Api v1"));
			}

			app.UseRabbitMQEventBus();

			app.UseRouting();

			app.UseAuthorization();

			app.UseCors(builder =>
				builder.WithOrigins("http://localhost:8080")
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowCredentials()
			);

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapHub<EnergyProductionHub>("/production-hub");
				endpoints.MapHub<EnergyProductionTotalsHub>("/production-totals-hub");
				endpoints.MapHub<EnergyConsumptionHub>("/consumption-hub");
				endpoints.MapHub<EnergyConsumptionTotalsHub>("/consumption-totals-hub");
				endpoints.MapHub<EnergyConsumptionHub>("/consumer-hub");
			});
		}
	}
}